This is a fresh start for my obsidian vault, with the purpose to store my personal setup securely inside a git project.

## Structure

I sort different note types into different folders on the root level:
- `📆 daily/*`
- `✅ gtd`
- `🧦 lists/*`
- `🧮 loom/*`
- `🔖 loose notes/*`
- `🌍 wiki/*`

My templates will be stored inside `_templates/`. The idea is to organise template in a similar manner as the common notes.

It's still questionable if I want to integrate other file types inside this vault.
